
/*

StabStabRevolution

Created by Robin Baumgarten, Maximilian Krauss, Michael Elwin Setiadi at the 2016 IGJam at Gamescom 2016.

Hardware: Teensy 3.2, Rotary Encoders, Vibration Motors, LED Disk (https://www.adafruit.com/product/2477)

License: MIT - https://opensource.org/licenses/MIT

*/

#include <FastLED.h>   
#include <Audio.h>
#include <Encoder.h>
#include <SPI.h>
#include <SD.h>

#define NUM_LEDS 255

CRGB leds[NUM_LEDS];
int blood[NUM_LEDS];
int c = 0;

Encoder p0(15, 16);
Encoder p1(18, 19);
Encoder p2(21, 22);

struct Dancer
{
	bool active;
	bool isPlayer;
	int playerId; 
	int ring;
	int pos;
	int oldPixel;
	int moveTimer;
	CRGB color;
	int hueVariation;
	int flashTime;
	int flashingTicks;

	int offbeat;
	long lastMove;

	// AI dancers
	int beatNoise;
	long nextBeatTime;
	int nextBeatId;
	int nextBeatDirection;

	// Player vars
	int prevEncoderPos;
	bool canAttack;
	bool buttonPressed;
	int vibrationTimer;
	int attackCooldown;
};
int dancerMinRing = 2; // smallest ring = 0;
int dancerMaxRing = 9;
int ledsPerRing[] = {1, 6, 12, 20, 24, 28, 32, 40, 44, 48};
int ringOffset[10];

Dancer dancers[50];
int activeDancers;
int beatCount = 0;
long lastBeat;
long nextBeat;
long nextnextBeat;
int nextBeatDirection;
int nextnextBeatDirection;
long lastTime = 0;
long currentTime;
int delta;
int dancerBrightness = 180;

bool gameOver = false;
bool gameOverAnimation = false;
long gameOverTimer = 0;
int gameOverFader = 255;

float volume = 0.6f;

static int LEFT = -1;
static int RIGHT = 1;
static int UP = 2;
static int DOWN = 3;

struct Attack
{
	bool active;
	int owner;
	int ring;
	int pos;
	int startTime;
	float speed;
	float extend;
	float maxExtend;
};

Attack attacks[20];

// AUDIO

elapsedMillis timer;
int sixteenth_notes=0;
const int max_notes=216;
float pitch=0.5;

// GUItool: begin automatically generated code
AudioSynthWaveform       beat;      //xy=391,302
AudioSynthWaveform       melody;      //xy=397,402
AudioSynthWaveform       sound_effects;
AudioMixer4              mixer1;         //xy=663,325
AudioOutputAnalog        dac1;           //xy=886,322
AudioConnection          patchCord1(beat, 0, mixer1, 1);
AudioConnection          patchCord2(melody, 0, mixer1, 0);
AudioConnection          patchCord3(sound_effects, 0, mixer1, 2);
AudioConnection          patchCord5(mixer1, dac1);


int beat_freqs[max_notes] = {
	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,

	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,

	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,

	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,

	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,

	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,

	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,

	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,

	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,
	200,0,1000,0,1000,0,
	200,0,0,0,0,0

}; 



int melody_freqs[max_notes]={

	0,0,0,0,0,0,
	0,0,0,0,0,0,
	0,0,0,0,0,0,
	0,0,200,0,1250,0,

	2000,2000,2000,2000,2000,2000,
	1500,1500,1500,1500,1500,1500,
	2500,2500,2500,2500,2500,2500,
	2250,2250,2250,2250,2250,2250,

	2000,2000,2000,0,2500,0,
	2000,2000,2000,0,2500,0,
	2000,2000,2000,2000,2000,2000,
	0,0,0,0,0,0,

	2000,2000,2000,2000,2000,2000,
	1500,1500,1500,1500,1500,1500,
	2000,2000,2000,2000,2000,2000,
	0,0,0,0,0,0,

	1500,1500,1500,1500,1500,1500,
	1000,1000,1000,1000,1000,1000,
	1500,1500,1500,1500,1500,1500,
	0,0,200,0,1250,0,

	2000,2000,2000,2000,2000,2000,
	1500,1500,1500,1500,1500,1500,
	2500,2500,2500,2500,2500,2500,
	2250,2250,2250,2250,2250,2250,

	2000,2000,2000,0,2500,0,
	2000,2000,2000,0,2500,0,
	2000,2000,2000,2000,2000,2000,
	0,0,1000,0,1500,0,

	1500,1500,1500,1500,1500,1500,
	1000,1000,1000,1000,1000,1000,
	1500,1500,1500,1500,1500,0,
	1500,1500,1500,0,2000,0,

	2000,2000,2000,0,1750,0,
	2750,2750,2750,0,2250,0,
	2000,2000,2000,2000,2000,0,
	2500,0,0,0,0,0
};



//this is for sound effects

boolean trigger_shot = false;
boolean trigger_gameover = false;
boolean trigger_death = false;

int shot_frequency = 0;
int gameover_frequency = 0;
int death_frequency = 0;

int death_ticks = 0;
int gameover_ticks = 0;



void loop_shot()
{
	if(trigger_shot)
	{
		trigger_shot=false;
		shot_frequency = 1500; 
	}
	if(shot_frequency > 0)
	{
		shot_frequency -= 500;
	}
}

void loop_death()
{

	death_ticks++;
	if(death_ticks == 6)
		death_frequency =0;

	if(trigger_death)
	{
		trigger_death=false;
		shot_frequency=0;
		death_frequency = 300;
		death_ticks = 0;
	}
}

void loop_gameover()
{
	gameover_ticks++;

	if (gameOver)
	{
		switch(gameover_ticks)
		{
			case 1: case 2: 
				gameover_frequency = 600; 
				break;
			case 4:  case 5: 
				gameover_frequency = 1200;
				break;
			case 7:  case 8: 
				gameover_frequency = 1800;
				break;
			  
			case 3: case 6: case 9: case 12: case 15: case 18:  
				gameover_frequency = 0;
				break;
			case 10: case 11: case 13: case 14: case 16: case 17:
				gameover_frequency = 1200;
				break;

			default:
				gameover_frequency=0;
				break;
		}
	}

	if(trigger_gameover)
	{
		trigger_gameover=false;
		shot_frequency=0;
		death_frequency=0;
		gameover_frequency=100;
	}
}

void loop_sound_effect()
{
	return;
	if(gameover_frequency > 0)
	{
		sound_effects.begin(volume,gameover_frequency, WAVEFORM_SQUARE);
	}
	else 
	{
		if(death_frequency> 0)
		{
			sound_effects.begin(volume,death_frequency, WAVEFORM_SQUARE);
		}
		else
		{
			if(shot_frequency > 0)
			{
				sound_effects.begin(volume * 1.5,shot_frequency, WAVEFORM_SQUARE);
			}
		}
	}
}

void loop_beat()
{
	beat.begin(volume * 0.8, beat_freqs[sixteenth_notes]*pitch, WAVEFORM_PULSE);
}

void loop_melody()
{
	melody.begin(volume * 0.8,melody_freqs[sixteenth_notes]*pitch, WAVEFORM_SINE);
}

//this function is the last to be defined and takes care of both music and sound effects
void loop_music()
{

	if(timer > 150) //trigger the next sixteenth note
	{
		timer=0;
		sixteenth_notes++;
		sixteenth_notes = sixteenth_notes % max_notes;
	}

	if(timer > 50)
	{
		loop_death();
		loop_shot();
		loop_gameover();
	}
	if(!gameOver)
	{ 
		loop_beat();
		loop_melody();  
	}
	loop_sound_effect();
}

// AUDIO END



void initAttacks()
{
	for(int i=0; i<20; i++)
	{
		attacks[i].active = false;    
	}
}

void initDancers(int count)
{
	activeDancers = count;
	for(int i=0; i<50; i++)
	{
		dancers[i].isPlayer = i < 3;
		dancers[i].playerId = (i < 3 ? i : -1);
		dancers[i].active = i < activeDancers;
		dancers[i].ring = random(dancerMinRing, dancerMaxRing);  
		if (i < 3)
			dancers[i].ring = i * 2 + 4;  

		dancers[i].pos = random(ledsPerRing[dancers[i].ring]);
		dancers[i].beatNoise = 10;
		dancers[i].nextBeatId = 0;
		dancers[i].nextBeatDirection = LEFT;
		dancers[i].nextBeatTime = nextBeat + random(-dancers[i].beatNoise, dancers[i].beatNoise);
		// dancers[i].color = i < 3 ? CRGB::Green : CRGB::Red; //CHSV(random(0, 255), 255, 255);
		dancers[i].color = CHSV(1, 255, dancerBrightness); //random(0, 255)
		dancers[i].hueVariation = (i < 3 ? 0 : random(10));
		dancers[i].flashTime = random(5000);
		dancers[i].offbeat = 0;

		dancers[i].buttonPressed = false;
		dancers[i].canAttack = false;
		dancers[i].prevEncoderPos = 0;
		dancers[i].vibrationTimer = 110;
		dancers[i].attackCooldown = 0;
	}

}

void init()
{
    for(int i=0; i<NUM_LEDS; i++)
    {
    	leds[i] = CRGB::Black;    
    	blood[i] = 0;
    }

    gameOver = false;
    gameOverAnimation = false;
    gameOverTimer = 0;
    gameOverFader = 255;
    beatCount = 0;

    nextBeat = millis() + 600;
    nextnextBeat = millis() + 1200;
    nextBeatDirection = LEFT;
    nextnextBeatDirection = RIGHT;
    lastTime = millis();
    initDancers(20);
}

void setup()
{
    Serial.begin(115200);
    LEDS.addLeds<APA102, 7, 14, BGR, DATA_RATE_MHZ(24)>(leds, NUM_LEDS);
    // LEDS.setBrightness(50);

    int offset = 0;
    for (int i = 9; i >= 0; i--)
    {
    	ringOffset[i] = offset;
    	offset += ledsPerRing[i];
    }

    AudioMemory(6);

    beat.pulseWidth(0.2);  
    dac1.analogReference(INTERNAL);

    pinMode(17, INPUT_PULLUP);
    pinMode(20, INPUT_PULLUP);
    pinMode(23, INPUT_PULLUP);

    pinMode(3, OUTPUT);
    pinMode(4, OUTPUT);
    pinMode(5, OUTPUT);

    init();
}


int actualLEDPos(int ring, int pos)
{
	return ringOffset[ring] + pos % ledsPerRing[ring];
}

void setActualPixel(int pixel, CRGB col)
{
	leds[pixel] = col;
}

void setPixel(int ring, int pos, CRGB col)
{
	if (ring < 0 || ring > 9) return;
	if (pos < 0) pos = ledsPerRing[ring] + pos;
	if (pos > ledsPerRing[ring]) pos -= ledsPerRing[ring];
	if (pos < 0 || pos > ledsPerRing[ring]) return;
	leds[actualLEDPos(ring, pos)] = col;
}

void beatNow()
{
	beatCount++;
	lastBeat = nextBeat;
	nextBeat = nextnextBeat;
	nextnextBeat = nextnextBeat + 600;
	nextBeatDirection = nextnextBeatDirection;
	nextnextBeatDirection = random(5) - 1;

}

int translateUp(int ring, int pos)
{
	if (ring >= 9) return pos;
	if (ring <= 0) return random(0, ledsPerRing[1]);
	return (pos * ledsPerRing[ring + 1]) / ledsPerRing[ring];
}

int translateDown(int ring, int pos)
{
	if (ring == 0) return 0;
	if (ring == 1) return 0;
	return (pos * ledsPerRing[ring - 1]) / ledsPerRing[ring];
}

int translateSideways(int ring, int pos, int dir)
{
	if (pos + dir < 0) return ledsPerRing[ring] - (pos + dir + 2);
	return (pos + dir) % ledsPerRing[ring];
}


void dance(int d)
{
	bool moved = true;
	int oldPixel = actualLEDPos(dancers[d].ring, dancers[d].pos);
	if (dancers[d].nextBeatDirection == UP && dancers[d].ring < 9)
	{
		dancers[d].pos = translateUp(dancers[d].ring, dancers[d].pos);
		dancers[d].ring++;
	}
	else if (dancers[d].nextBeatDirection == DOWN && dancers[d].ring > 1)
	{
		dancers[d].pos = translateDown(dancers[d].ring, dancers[d].pos);
		dancers[d].ring--;
	}
	else if (dancers[d].nextBeatDirection == LEFT || dancers[d].nextBeatDirection == RIGHT)
	{
		dancers[d].pos = translateSideways(dancers[d].ring, dancers[d].pos, dancers[d].nextBeatDirection);
	}
	else moved = false;

	if (moved)
	{
		dancers[d].oldPixel = oldPixel;
		dancers[d].moveTimer = 250;


		int beatDistance = min(abs(currentTime - lastBeat), abs(currentTime - nextBeat));
		if (beatDistance < 200 && (currentTime - dancers[d].lastMove) > 250)
		{
			// good
			dancers[d].offbeat *= 0.8;
			if (dancers[d].offbeat < 0) dancers[d].offbeat = 0;
		}
		else
		{
			// bad
			dancers[d].offbeat+=2;
			if (dancers[d].nextBeatDirection == UP || dancers[d].nextBeatDirection == DOWN)
				dancers[d].offbeat+=4;

			if (dancers[d].offbeat > 120) dancers[d].offbeat = 120;
		}

		dancers[d].color = CHSV((beatCount + dancers[d].offbeat + dancers[d].hueVariation) % 255, 255, dancerBrightness); 

		dancers[d].lastMove = currentTime;
	}
}

int getEncoderPos(int pid)
{
	if (pid == 0) return p0.read()/4;
	if (pid == 1) return p1.read()/4;
	return p2.read()/4;
	// return 0;
}

bool isButtonPressed(int pid)
{
	if (pid == 0) return digitalRead(17) == LOW;
	if (pid == 1) return digitalRead(20) == LOW;
	return digitalRead(23) == LOW;
	// return false;
}

void vibrate(int id, bool on)
{
	// activate vibrations here
	if (id == 0) digitalWrite(3, on ? HIGH : LOW);
	if (id == 1) digitalWrite(4, on ? HIGH : LOW);
	if (id == 2) digitalWrite(5, on ? HIGH : LOW);
}

int getDistance(int ring, int p1, int p2)
{
	int dist = abs(p1 - p2);
	if (dist > ledsPerRing[ring] / 2) dist = ledsPerRing[ring] - dist;
	return dist;
}

void hitPlayer(int id)
{
	Serial.print("Player ");
	Serial.print(id);
	Serial.println(" hit!");
	dancers[id].active = false;
	dancers[id].vibrationTimer = 1000;
	blood[actualLEDPos(dancers[id].ring, dancers[id].pos)] = 30;

	int deadPlayers = 0;
	for (int i = 0; i < 3; i++)
	{
		if (!dancers[i].active) deadPlayers++;
	}
	if (deadPlayers >= 2)
	{
		gameOver = true;
		gameOverTimer = currentTime;
		trigger_gameover = true;
	}
	else
	{
		trigger_death = true;
	}
}

void updateAttacks()
{
	for(int i=0; i<20; i++)
	{
		if (!attacks[i].active) continue;
    	sound_effects.begin(volume * 1.5, attacks[i].extend * 100 + 1000, WAVEFORM_SQUARE);

		if (attacks[i].extend > attacks[i].maxExtend)
		{
			attacks[i].active = false;
    		sound_effects.begin(0,gameover_frequency, WAVEFORM_SQUARE);
		}

		attacks[i].extend += attacks[i].speed * delta;

		for(int p=0; p<3; p++)
		{
			if (attacks[i].owner == p) continue;
			if (dancers[p].active && attacks[i].ring == dancers[p].ring 
				&& getDistance(attacks[i].ring, attacks[i].pos, dancers[p].pos) <= attacks[i].extend)
			{
				// getting hit!
				hitPlayer(p);
				dancers[attacks[i].owner].vibrationTimer = 500;
			}
		    
		}
	}
}

void drawAttacks()
{

	for(int i=0; i<20; i++)
	{
		if (!attacks[i].active) continue;
		for(int j=0; j< attacks[i].extend; j++)
		{
			setPixel(attacks[i].ring, attacks[i].pos + j, CHSV(0, 0, (j * 255 /attacks[i].extend)));		    
			setPixel(attacks[i].ring, attacks[i].pos - j, CHSV(0, 0, (j * 255 /attacks[i].extend)));		    
		}
	}
}

void attack(int pid)
{
	int attackId = -1;
	for(attackId=0; attackId<20; attackId++)
	{
	    if (!attacks[attackId].active) break;
	}
	if (attackId == -1) return;

	attacks[attackId].active = true;
	attacks[attackId].owner = pid;
	attacks[attackId].ring = dancers[pid].ring;
	attacks[attackId].pos = dancers[pid].pos;
	attacks[attackId].startTime = currentTime;

	attacks[attackId].speed = 0.05;
	attacks[attackId].extend = 1;
	attacks[attackId].maxExtend = min(7, max(3, ledsPerRing[dancers[pid].ring] / 4));

	dancers[pid].offbeat += 20;
	dancers[pid].color = CHSV((beatCount + dancers[pid].offbeat) % 255, 255, dancerBrightness); 

	dancers[pid].attackCooldown = 2000;

	trigger_shot = true;
}

void updatePlayers(int id)
{
	int currentEncoder = getEncoderPos(id);
	bool buttonPressed = isButtonPressed(id);



	if (!dancers[id].active) 
	{
		return;
	}

	dancers[id].attackCooldown -= delta;
	bool reloadVibrate = false;
	if (dancers[id].attackCooldown < 0 && dancers[id].attackCooldown > -100) reloadVibrate = true;
	if (dancers[id].attackCooldown < -200 && dancers[id].attackCooldown > -300) reloadVibrate = true;
	
	vibrate(id, reloadVibrate);

	if (dancers[id].vibrationTimer > 0)
	{
		dancers[id].vibrationTimer -= delta;
		vibrate(id, true);	
	}
	// else
		// vibrate(id, false);	

	if (buttonPressed != dancers[id].buttonPressed)
	{
		dancers[id].buttonPressed = buttonPressed;
		if (buttonPressed)
		{
			// started pressing!
			dancers[id].canAttack = true;
		}
		else
		{
			// stopped pressing
			if (dancers[id].canAttack && dancers[id].attackCooldown < 0)
			{
				attack(id);
			}
		}
	
	}


	if (dancers[id].prevEncoderPos != currentEncoder)
	{
		// encoder pos changed!
		int diff = currentEncoder - dancers[id].prevEncoderPos;
		if (buttonPressed)
		{
			dancers[id].canAttack = false;
			if (diff > 0)
			{
				dancers[id].nextBeatDirection = UP;
			}
			else
			{
				dancers[id].nextBeatDirection = DOWN;
			}
		}
		else
		{
			if (diff > 0)
			{
				dancers[id].nextBeatDirection = RIGHT;
			}
			else
			{
				dancers[id].nextBeatDirection = LEFT;
			}
		}
		dance(id);
		dancers[id].prevEncoderPos = currentEncoder;
	}
}


void updateDancers()
{
	for(int i=0; i<activeDancers; i++)
	{
		if (i < 3) 
		{
			updatePlayers(i);
			continue;
		}

		if (!dancers[i].active) continue;
				
		if (dancers[i].nextBeatTime < millis())
		{
			// depending if the dancer dances slightly before or after the actual beat, 
			// we need to look for the next beat or the one after that
			if (dancers[i].nextBeatId == beatCount - 1)
			{
				dancers[i].nextBeatTime = nextBeat + random(-dancers[i].beatNoise, dancers[i].beatNoise);
				//dancers[i].nextBeatDirection = nextBeatDirection;
				dancers[i].nextBeatDirection = random(5) - 1;
				if (dancers[i].nextBeatDirection == 0) dancers[i].nextBeatDirection = LEFT;
			}
			else
			{
				dancers[i].nextBeatTime = nextnextBeat + random(-dancers[i].beatNoise, dancers[i].beatNoise);
				//dancers[i].nextBeatDirection = nextnextBeatDirection;
				dancers[i].nextBeatDirection = random(5) - 1;
				if (dancers[i].nextBeatDirection == 0) dancers[i].nextBeatDirection = LEFT;
			}
			dance(i);
			dancers[i].nextBeatId++;
		}
	}
}

void drawDancers()
{
	for(int i=activeDancers - 1; i>= 0; i--)
	{
		if (!dancers[i].active) continue;
		if (gameOver && i > 2) continue;
		dancers[i].flashTime -= delta;
		if (dancers[i].flashTime < 0)
		{
			dancers[i].flashTime += 5000;
			dancers[i].flashingTicks = 30;
		    if (i < 3 && !gameOver)
		    {
		    	// vibrate
		    	dancers[i].vibrationTimer = 70;
		    }
		}
		
		if (dancers[i].flashingTicks > 0)
		{
			dancers[i].flashingTicks--;
		    setPixel(dancers[i].ring, dancers[i].pos, CRGB::White);
		}
		else
		{
		    if (dancers[i].moveTimer > 0)
		    {
		    	dancers[i].moveTimer -= delta * 4;
		    	if (gameOver)
		    		dancers[i].color = CRGB::White;
		    	setActualPixel(dancers[i].oldPixel, dancers[i].color % (dancers[i].moveTimer));
		    	setPixel(dancers[i].ring, dancers[i].pos, dancers[i].color % (255- dancers[i].moveTimer));

		    }
		    else
		    	setPixel(dancers[i].ring, dancers[i].pos, dancers[i].color);

		}
	}
}


void drawBackground()
{

	int lastBeatTime = currentTime - lastBeat;
	for(int i=0; i<NUM_LEDS; i++)
	{
	    if (blood[i] > 0)
	    	leds[i] = CRGB(blood[i] * 5, 0, 0);// CHSV((millis()/100) % 255, 255, 15);//CRGB(0,0,5);
	    else
	    {
	    	if (gameOverAnimation)
	    	{
	    		leds[i] = CHSV(i + currentTime, 255, min(150, (currentTime - gameOverTimer - 1000)/30)); //CRGB(0, 0, max(3, 10 - lastBeatTime / 20));// CHSV((millis()/100) % 255, 255, 15);//CRGB(0,0,5);
    			if (millis() - gameOverTimer < 5000)
    			{
	    			sound_effects.begin(volume * 1.1f, (millis() - gameOverTimer) % 500 + 1500 + (millis() - gameOverTimer) / 100, WAVEFORM_SINE);
 					beat.begin(0, 0, WAVEFORM_PULSE);
 					melody.begin(0, 0, WAVEFORM_PULSE);

    			}
    			else
	    			sound_effects.begin(0.0, 1, WAVEFORM_SINE);


	    	}
	    	else
	    		// leds[i] = CHSV(beatCount * 10, 255, max(3, (i % 255) /20 + 10 - lastBeatTime / 20));// CHSV((millis()/100) % 255, 255, 15);//CRGB(0,0,5);
	    		leds[i] = CRGB(0, 0, max(3, (i % 255) /15 + 10 - lastBeatTime / 20));// CHSV((millis()/100) % 255, 255, 15);//CRGB(0,0,5);

	    }
	    if (blood[i] > 5 && random(100) > 98)
	    {
	    	if (i > 0 && i < NUM_LEDS - 1)
	    	{
	    		blood[i - 1]++;
	    		blood[i + 1]++;
	    		blood[i] -=2;
	    	}
	    }
	}

}

void loop()
{
	currentTime = millis();
	delta = currentTime - lastTime;

	if (gameOver && (currentTime - gameOverTimer > 1000))
	{
		drawBackground();
		updateDancers();
		drawDancers();
		gameOverAnimation = true;

		if ((isButtonPressed(0) && isButtonPressed(1) && isButtonPressed(2)) ||  (currentTime - gameOverTimer > 15000) )
			init();

	}
	else if (delta > 0)
	{
		lastTime = currentTime;
		if (currentTime > nextBeat)
		{
			beatNow();
			Serial.println("BEAT");
		}
		updateDancers();
		updateAttacks();
		drawBackground();
		drawDancers();
		drawAttacks();

	}
  	loop_music();

	FastLED.show(); 

}