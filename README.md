#  StabStabRevolution #

## A local multiplayer rhythm stealth assassination game running on an Arduino and an LED disk. ##

Created by Robin Baumgarten, Maximilian Krauss, Michael Elwin Setiadi at the 2016 IGJam at Gamescom 2016.

Hardware: Teensy 3.2, Rotary Encoders, Vibration Motors, LED Disk (https://www.adafruit.com/product/2477)

License: MIT - https://opensource.org/licenses/MIT